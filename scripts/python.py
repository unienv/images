from base import clone

clone('python:3.12')
clone('python:3.12-alpine')
clone('python:3.11')
clone('python:3.11-alpine')
clone('python:3.10')
clone('python:3.10-alpine')
clone('python:3.9')
clone('python:3.9-alpine')