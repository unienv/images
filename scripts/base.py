import os


def clone(image):
    os.system(f'docker pull {image}')
    os.system(f'docker tag {image} registry.gitlab.com/unienv/images/{image}')
    os.system(f'docker push registry.gitlab.com/unienv/images/{image}')