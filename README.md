# Docker images

[Доступные образы представлены здесь](https://gitlab.com/unienv/images/container_registry)

## Локальное использование

Преварительно авторизуйтесь в Gitlab Registry. Вместо пароля [создайте access token](https://gitlab.com/-/user_settings/personal_access_tokens) с доступом `read_registry` и вводите его.

```bash
docker login registry.gitlab.com
```

## Использование в CI

Добавить в `.gitlab-ci.yml`:

```yml
before_script:
  - echo "$CI_REGISTRY_PASSWORD" | docker login $CI_REGISTRY -u $CI_REGISTRY_USER --password-stdin
```
